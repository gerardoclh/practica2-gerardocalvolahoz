package main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JProgressBar;
import javax.swing.JList;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextPane;
import javax.swing.JButton;
import javax.swing.JToolBar;
import javax.swing.JPasswordField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import javax.swing.ButtonGroup;
import javax.swing.JTabbedPane;
import javax.swing.JComboBox;
import javax.swing.JMenuItem;
import javax.swing.DefaultComboBoxModel;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JCheckBox;
import java.awt.Canvas;
import java.awt.Panel;
import javax.swing.JTextField;

public class VentanaEclipse extends JFrame {

	private JPanel contentPane;
	private JPasswordField pwdContrasea;
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanaEclipse frame = new VentanaEclipse();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public VentanaEclipse() {
		setTitle("Alta de usuario ");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 278);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnEdicion = new JMenu("Edicion");
		menuBar.add(mnEdicion);
		
		JMenuItem mntmUsuarios = new JMenuItem("Usuarios");
		mnEdicion.add(mntmUsuarios);
		
		JMenu mnAyuda = new JMenu("Ayuda");
		menuBar.add(mnAyuda);
		
		JMenuItem mntmSoporte = new JMenuItem("Soporte");
		mnAyuda.add(mntmSoporte);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Otro");
		buttonGroup.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.setBounds(350, 95, 60, 23);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnNewRadioButton_1 = new JRadioButton("M");
		buttonGroup.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.setBounds(309, 95, 39, 23);
		contentPane.add(rdbtnNewRadioButton_1);
		
		JRadioButton rdbtnNewRadioButton_2 = new JRadioButton("H");
		buttonGroup.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.setBounds(277, 95, 47, 23);
		contentPane.add(rdbtnNewRadioButton_2);
		
		JSpinner spinner = new JSpinner();
		spinner.setBounds(374, 11, 39, 20);
		contentPane.add(spinner);
		
		JTextPane txtpnSelecciona = new JTextPane();
		txtpnSelecciona.setEditable(false);
		txtpnSelecciona.setText("Edad:");
		txtpnSelecciona.setBounds(278, 11, 70, 20);
		contentPane.add(txtpnSelecciona);
		
		JButton btnNewButton = new JButton("Crear usuario");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnNewButton.setBounds(20, 161, 117, 23);
		contentPane.add(btnNewButton);
		
		pwdContrasea = new JPasswordField();
		pwdContrasea.setText("Contrase\u00F1a");
		pwdContrasea.setBounds(149, 11, 117, 20);
		contentPane.add(pwdContrasea);
		
		JFormattedTextField frmtdtxtfldUsuario = new JFormattedTextField();
		frmtdtxtfldUsuario.setText("Usuario");
		frmtdtxtfldUsuario.setBounds(20, 11, 117, 20);
		contentPane.add(frmtdtxtfldUsuario);
		
		JTextPane textPane = new JTextPane();
		textPane.setBounds(20, 42, 246, 76);
		contentPane.add(textPane);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(20, 195, 404, 14);
		contentPane.add(progressBar);
		
		JCheckBox chckbxAceptaLosTerminos = new JCheckBox("Acepto los terminos");
		chckbxAceptaLosTerminos.setBounds(271, 123, 142, 23);
		contentPane.add(chckbxAceptaLosTerminos);
		
		textField = new JTextField();
		textField.setBounds(63, 124, 203, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JButton btnSalir = new JButton("Salir");
		btnSalir.setBounds(307, 161, 117, 23);
		contentPane.add(btnSalir);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBounds(163, 161, 117, 23);
		contentPane.add(btnLimpiar);
		
		JLabel lblDni = new JLabel("DNI:");
		lblDni.setBounds(20, 129, 46, 14);
		contentPane.add(lblDni);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(277, 74, 46, 14);
		contentPane.add(lblSexo);
	}
}
