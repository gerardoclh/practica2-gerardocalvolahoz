package main;
import java.util.Scanner;

import libreria.Libreria;


public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int [] v = new int[5];
		int opcion=0;
		
		do{
			//Menu
			System.out.println("*** Seleciona una opcion ***");
			System.out.println("1-Ejecutar suma");
			System.out.println("2-Ejecutar rellenar vector");
			System.out.println("3-Ejecutar multiplicar");
			System.out.println("4-Ejecutar piramide");
			opcion= in.nextInt();
			
			//Seleccion de opcion
			if (opcion==1){
				System.out.println(Libreria.suma(4, 6));
			}
			if (opcion==2){
				Libreria.rellenarVector(v);
			}
			if (opcion==3){
				System.out.println(Libreria.multiplica(2, 3));
			}
			if (opcion==4){
				Libreria.pintarPiramide();
			}
			else{
				System.out.println("RIP");
				
			}
		}while(opcion!=5);
		

		
		
	}

}
